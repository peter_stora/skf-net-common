
/*
These two classes can be used to register dependecies in WCF application using Practices.Unity

To inject dependecies of WCF application, this snippet of code shall be pasted where the application is going to be hosted e.g. console application.

In the constroctor of the UnityServiceHostFactory class all the dependecies are registred; each repository with its service/interface is registred using the RegisterType method provided b the UnityContainer class (Unity.dll)

To run the UnityServiceHostFactory, just use the following in the main method for a console application:

UnityServiceHostFactory sFactory = new UnityServiceHostFactory();

ServieHost serviceHost = sFactory.CreateServiceHostWithType(typeof(Wcf.ServiceName), baseAddresses)
serviceHost.Open(), to run the service.

 */
    
public class UnityServiceHostFactory : ServiceHostFactory
{
    private static IUnityContainer mContainer;
    public UnityServiceHostFactory()
    {
        // create you container and register all the types

        mContainer = new UnityContainer();
        mContainer.RegisterType<IService1, Service1>(new ContainerControlledLifetimeManager())
            .RegisterType<IAlarmsRepository, AlarmsRepository>(new HierarchicalLifetimeManager())
            .RegisterType<IMachineRepository, MachineRepository>(new HierarchicalLifetimeManager())
            .RegisterType<IMessageRepository, MessageRepository>(new HierarchicalLifetimeManager())
            .RegisterType<INoteRepository, NoteRepository>(new HierarchicalLifetimeManager())
            .RegisterType<IPointRepository, PointRepository>(new HierarchicalLifetimeManager())
            .RegisterType<IMeasurementRepository, MeasurementRepository>(new HierarchicalLifetimeManager())
            .RegisterType<IServiceBLLogg, ServiceBLLogg>(new HierarchicalLifetimeManager())
            .RegisterType<IMessageWorkgoup, MessageWorkgoup>(new HierarchicalLifetimeManager())
            .RegisterType<PointSetup>(new InjectionConstructor());
    }

    protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
    {
        return new UnityServiceHost(mContainer, serviceType, baseAddresses);
    }

    public ServiceHost CreateServiceHostWithType(Type serviceType, Uri[] baseAddresses)
    {
        return new UnityServiceHost(mContainer, serviceType, baseAddresses);
    }

    public ServiceHost CreateServiceHostWithType(Type serviceType)
    {
        return new UnityServiceHost(mContainer, serviceType);
    }
}

public partial class UnityServiceHost : ServiceHost
{
    public UnityServiceHost(IUnityContainer container, Type serviceType, params Uri[] baseAddresses)
        : base(container.Resolve(serviceType), baseAddresses)
    {
    }

    public UnityServiceHost(IUnityContainer container, Type serviceType)
        : base(container.Resolve(serviceType))
    {
    }
}